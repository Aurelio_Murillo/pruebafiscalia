<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</head>
<body>
<table class="table">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">nombre</th>
      <th scope="col">Email</th>
      <th scope="col">Password</th>
      <th scope="col">Fecha de Registro</th>
      <th scope="col">Modificaion</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
      @foreach($listado as $l)
            <tr>
                <td>{{$l->id}}</td>
                <td>{{$l->name}}</td>
                <td>{{$l->email}}</td>
                <td>{{$l->password}}</td>
                <td>{{$l->created_at}}</td>
                <td>{{$l->updated_at}}</td>
                <td><a class="btn btn-warning">Editar</a> <a class="btn btn-success">Detalle</a> </td>
            </tr>
      @endforeach
  </tbody>
</table>
<a  class="btn btn-danger"  href="Nuevo" role="button">Nuevo usuario</a>
</body>
</html>