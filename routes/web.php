<?php

use App\Http\Controllers\DashController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/',[DashController::class,'listados']);
/*

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

*/
//auth route for both 
Route::group(['middleware' => ['auth']], function() { 
    Route::get('/dashboard', 'App\Http\Controllers\DashController@index')->name('dashboard');
});
//for supervisor
Route::group(['middleware' => ['auth', 'role:Supervisor']], function() { 
    Route::get('/dashboard/myprofile', 'App\Http\Controllers\DashController@myprofile')->name('dashboard.myprofile');
});

// for SuperUsuario
Route::group(['middleware' => ['auth', 'role: superuser']], function() { 
    Route::get('/dashboard/postcreate', 'App\Http\Controllers\DashController@postcreate')->name('dashboard.postcreate');
});

require __DIR__.'/auth.php';
