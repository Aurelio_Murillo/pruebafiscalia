<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashController extends Controller
{
    public function index()
   {
       if(Auth::user()->hasRole('Supervisor')){
           return view('SupervisorDash');
       }elseif(Auth::user()->hasRole('superuser')){
           return view('superUsuarioDash');
       }elseif(Auth::user()->hasRole('Admin')){
        return view('AdminDash');
    }
   }
   public function myprofile()
   {       $query=DB::table('dbo.users')
    ->get();
    return  view('myprofile',['lista'=>$query]);  
   }

   public function postcreate()
   {
    return view('postcreate');
   }
    public function listados(){
        $query=DB::table('dbo.users')
        ->get();
        return  view('myprofile',['lista'=>$query]);
   }
   
}
